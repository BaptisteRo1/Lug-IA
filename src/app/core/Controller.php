<?php

class Controller
{
    public function model($model)
    {
        if (file_exists('../app/models/' . $model . '.php')) {
            require_once '../app/models/' . $model . '.php';
            return new $model();
        } else {
            die('Model does not exist.');
        }
    }

    public function view($view, $datas = [])
    {
        if (file_exists('../app/views/' . $view . '.php')) {
            require_once '../app/views/' . $view . '.php';
            $class = $this->getClass($view);
            return new $class($datas);

        } else {
            die('View does not exist');
        }
    }

    public function getUserSession()
    {
        if (isset($_SESSION['user'])) {
            $this->model('User');
            return unserialize($_SESSION['user']);
        } else {
            return null;
        }
    }

    public function getBillingSession()
    {
        if (isset($_SESSION['billing'])) {
            $this->model('Billing');
            return unserialize($_SESSION['billing']);
        } else {
            return null;
        }
    }

    public function updateUser($user)
    {
        $userInfos = $user->userToarray();
        $database = $this->model('Database');
        $database->updateUser($userInfos);
        $_SESSION['user'] = serialize($user);
    }

    public function getClass($path)
    {
        $class = explode('/', $path);
        return $class[count($class) - 1];
    }
}