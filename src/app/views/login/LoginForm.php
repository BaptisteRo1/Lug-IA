<?php

echo '<link rel="stylesheet" type="text/css" href="../public/assets/css/style.css">';

class LoginForm
{
    public $errors = [];

    public function __construct($datas)
    {
        $this->errors = $datas;
    }

    public function display()
    {
        echo '<div class="form">';
        echo '<h1>Login</h1>';
        echo '<form method="post">';
        echo '<input type="email" name="email" placeholder="Email" value="" required>';
        echo '<input type="password" name="password" placeholder="Password" value="" required>';
        echo '<input type="submit" name="submit" value="Submit" class="submit">';
        echo '<a href="register" class="loginRegisterButton"><br></br>Register</a>';
        echo '</form>';
        echo '</div>';
    }

    public function displayErrors()
    {
        if (!empty($this->errors)) {
            foreach ($this->errors as $error) {
                echo '<p style="color: red;">' . $error . '</p>';
            }
        }
    }
}
