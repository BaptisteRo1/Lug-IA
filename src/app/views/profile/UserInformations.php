<?php

echo '<link rel="stylesheet" type="text/css" href="../public/assets/css/style.css">';

class UserInformations
{
    public function display($userInformations) {

        $imgPath = '../public/assets/img/avatars/' . $userInformations['avatar'] . '.png';

        echo '<div class="form">';
        echo '<h1>Profile</h1>';

        echo '<br><br><form method="post">';

        echo '<br><label for="first_name">First name</label>';
        echo '<br><input type="text" name="firstname" value="' . $userInformations['first_name'] . '" required>';

        echo '<br><br><label for="last_name">Last name</label>';
        echo '<br><input type="text" name="lastname" value="' . $userInformations['last_name'] . '" required>';

        echo '<br><br><label for="gender">Gender</label>';
        echo '<br><input type="text" name="gender" value="' . $userInformations['gender'] . '" required>';

        echo '<br><br><label for="avatar">Avatar</label>';
        echo '<br><br><select name="avatar" id="profileSelect">';
        echo '<option value="' . $userInformations['avatar'] . '">' . $userInformations['avatar'] . '</option>';
        echo '<option value="mew">Mew</option>';
        echo '<option value="mewtwo">Mewtwo</option>';
        echo '<option value="entei">Entei</option>';
        echo '<option value="raikou">Raikou</option>';
        echo '<option value="suicune">Suicune</option>';
        echo '<option value="ho_oh">Ho oh</option>';
        echo '<option value="celebi">Celebi</option>';
        echo '<option value="rayquaza">Rayquaza</option>';
        echo '<option value="jirachi">Jirachi</option>';
        echo '<option value="manaphy">Manaphy</option>';
        echo '<option value="shaymin">Shaymin</option>';
        echo '<option value="victini">Victini</option>';
        echo '</select><br>';
        echo '<input type="submit" name="submit" value="Modify profile" class="submit">';
        echo '</form>';

        echo '</div>';
    }
}