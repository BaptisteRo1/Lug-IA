<?php

class AccountingSelector
{
    public function display($accounting = null)
    {
        echo '<div class="accountingSelector">';
        echo '<form method="post" class="form">';

        if($accounting != null) echo '<h1>Accounting of year ' . $accounting['year'] . '</h1>';
         else echo '<h1>Accounting</h1>';

        echo '<br><br><select name="accountingYear">';
        echo '<option value="2022">2022</option>';
        echo '<option value="2023">2023</option>';
        echo '</select><br>';
        echo '<input type="submit" name="accounting" value="Accounting">';

        if($accounting != null) {
            echo '<p>Total Spent : ' . $accounting['total_spent'] . '€</p>';
            echo '<p><br>Total Earned : ' . $accounting['total_earned'] . '€</p>';
            echo '<p><br>Total Profit : ' . $accounting['total_profit'] . '€</p>';
        }

        echo '</form>';
        echo '</div>';
    }
}