<?php

class StockManagement
{
    public function display($sortOption, $images, $ids, $stocks)
    {
        echo '<p class="sortOption">' . $sortOption . '</p>';
        echo "<div class='images'>";
        for ($i = 0; $i < count($images); $i++) {
            if (($i % 8 == 0 && $i > 0) || $i == 0) {
                echo "</div><div class='divGameImages'>";
            }
            $imgPath = '../public/assets/img/covers/' . $images[$i];
            echo '<a href="product/' . $ids[$i] . '">';

            echo '<div class="adminGame">';
            echo '<img src=' . $imgPath . ' class="adminGameCover">';

            echo '<div class="basketManagementButtons">';
            echo '<a href="' . '../public/admin/' . 'removeGameStock/' . $ids[$i] . '"><button class="minus">-</button></a>';
            echo '<span class="stockQuantity">' . $stocks[$i] . '</span>';
            echo '<a href="' . '../public/admin/' . 'addGameStock/' . $ids[$i] . '"><button class="plus">+</button></a>';
            echo '</div>';

            echo '</div>';

            echo '</a>';
        }
        echo "</div>";
    }
}