<?php

class AddGameForm
{
    public $errors = [];

    public function __construct($datas)
    {
        $this->errors = $datas;
    }

    public function display()
    {
        echo '<div class="form" id="addGameForm">';
        echo '<h1>Add Game</h1><br>';
        echo '<form method="post">';

        echo '<input type="text" name="title" placeholder="Title" value="" required><br>';
        echo '<input type="number" name="purchasePrice" placeholder="Purchase price" value="" step="any" required><br>';
        echo '<input type="number" name="sellingPrice" placeholder="Selling price" value="" step="any" required><br>';
        echo '<textarea name="description" placeholder="Description" required></textarea><br>';
        echo '<input type="date" name="releaseDate" placeholder="Release date" value="" required><br>';

        echo '<select name="console" required>';
        echo '<option value="Game Boy">Game Boy</option>';
        echo '<option value="Game Boy Advance">Game Boy Advance</option>';
        echo '<option value="Game Cube">Game Cube</option>';
        echo '<option value="Nintendo 64">Nintendo 64</option>';
        echo '<option value="Nintendo DS">DS</option>';
        echo '<option value="Nintendo 3DS">Nintendo 3DS</option>';
        echo '<option value="Wii">Wii</option>';
        echo '<option value="Wii U">Wii U</option>';
        echo '<option value="Switch">Switch</option>';
        echo '</select><br>';

        echo '<select name="generation" required>';
        echo '<option value="I">I</option>';
        echo '<option value="II">II</option>';
        echo '<option value="III">III</option>';
        echo '<option value="IV">IV</option>';
        echo '<option value="V">V</option>';
        echo '<option value="VI">VI</option>';
        echo '<option value="VII">VII</option>';
        echo '<option value="VIII">VIII</option>';
        echo '<option value="IX">IX</option>';
        echo '</select><br>';

        echo '<select name="series" required>';
        echo '<option value="Main games">Main game</option>';
        echo '<option value="3D RPG">3D RPG</option>';
        echo '<option value="3D Battle">3D Battle</option>';
        echo '<option value="Mystery Dungeon">Mystery Dungeon</option>';
        echo '<option value="Ranger">Ranger</option>';
        echo '<option value="Pokepark">Pokepark</option>';
        echo '</select><br>';

        echo '<input type="number" name="sellerNote" placeholder="Seller note" value="" min="1" max="10"><br>';

        echo '<label for="image">Image (400 x 300px)</label><br>';
        echo '<input type="file" name="image" accept="image/png" required><br>';
        echo '<input type="submit" name="submitGame" value="Submit">';
        echo '</form>';
        echo '</div>';
    }

    public function displayErrors()
    {
        if (!empty($this->errors)) {
            foreach ($this->errors as $error) {
                echo '<p style="color: red;">' . $error . '</p>';
            }
        }
    }

    public function displaySuccess()
    {
        echo '<p style="color: green;">Game added successfully !</p>';
    }
}