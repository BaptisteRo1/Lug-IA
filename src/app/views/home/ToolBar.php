<?php
echo '<link rel="stylesheet" type="text/css" href="../public/assets/css/style.css">';

class ToolBar
{
    public function display($avatar = 'eevee')
    {
        echo '<div class="toolbar">';

        echo '<div class="sortOptions">';
        echo '<form method="post">';
        echo '<input type="submit" name="generation" value="generation">';
        echo '<input type="submit" name="console" value="console">';
        echo '<input type="submit" name="series" value="series">';
        echo '</form>';
        echo '</div>';

        echo '<div class="searchBar">';
        echo '<form method="post">';
        echo '<input type="text" name="search" placeholder="Search...">';
        echo '<input type="submit" value="Search">';
        echo '</form>';
        echo '</div>';

        echo '<a href="basket">';
        echo '<img src="../public/assets/img/basket2.png" alt="basket" class="basket">';
        echo '</a>';

        $avatar .= '.png';
        echo '<a href="profile">';
        echo '<img src="../public/assets/img/avatars/' . $avatar . '" alt="avatar" class="avatar">';
        echo '</a>';

        echo '</div>';
    }
}