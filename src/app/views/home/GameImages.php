<?php

echo '<link rel="stylesheet" type="text/css" href="../public/assets/css/style.css">';

class GameImages
{
    public function display($sortOption, $images, $ids, $available)
    {
        echo '<p class="sortOption">' . $sortOption . '</p>';
        echo "<div class='images'>";
        for ($i = 0; $i < count($images); $i++) {
            if (($i % 4 == 0 && $i > 0) || $i == 0) {
                echo "</div><div class='divGameImages'>";
            }
            $imgPath = '../public/assets/img/covers/' . $images[$i];
            if (!$available[$i]) {echo '<img src=' . $imgPath . ' class="gameImages grayscale">';
            } else {
                echo '<a href="product/' . $ids[$i] . '">';
                echo '<img src=' . $imgPath . ' class="gameImages available">';
                echo '</a>';
            }
        }
        echo "</div>";
    }
}