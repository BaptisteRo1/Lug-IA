<?php


class BasketInformations
{
    public function display($basketInformations)
    {
        $totalPrice = $basketInformations['total_price'];
        $creationDate = $basketInformations['creation_date'];

        echo '<div class="basketInformations">';
        echo '<h1>' . 'Basket' . '</h1>';
        echo '<p>Creation date: ' . $creationDate . '</p>';
        echo '<p>Total price: ' . $totalPrice . '€</p>';
        echo '</div>';
    }
}