<?php

class BasketManagement
{
    public function display($gameId, $quantity, $basketPath, $cover)
    {
        if($cover) echo '<img src="' . '../public/assets/img/covers/' . $cover . '" alt="cover" class="basketCover">';

        echo '<div class="basketManagementButtons">';
        echo '<a href="' . $basketPath . 'removeGame/' . $gameId . '"><button class="minus">-</button></a>';
        echo '<span class="quantity">' . $quantity . '</span>';
        echo '<a href="' . $basketPath . 'addGame/' . $gameId . '"><button class="plus">+</button></a>';
        echo '</div>';
    }
}