<?php

class OrderButton
{
    public function display($noGamesInBasket)
    {
        if ($noGamesInBasket) {
            echo '<a href="../public/basket/order/"><button class="button" id="orderButton" disabled>Order</button></a>';
        } else echo '<a href="../public/basket/order/"><button class="button" id="orderButton">Order</button></a>';
    }
}