<?php

echo '<link rel="stylesheet" type="text/css" href="../../../public/assets/css/style.css">';

class OrderForm
{
    public function display() {
        echo '<form method="post">';
        echo '<div class="form">';
        echo '<h1>Shipping Informations</h1>';
        echo '<input type="text" name="firstName" placeholder="First Name" value="" required>';
        echo '<input type="text" name="lastName" placeholder="Last Name" value="" required>';
        echo '<input type="text" name="address" placeholder="Address" value="" required>';
        echo '<input type="text" name="city" placeholder="City" value="" required>';
        echo '<input type="text" name="zipCode" placeholder="Zip Code" value="" required>';
        echo '<input type="text" name="country" placeholder="Country" value="" required>';
        echo '<input type="text" name="phone" placeholder="Phone" value="" required>';
        echo '<input type="submit" name="submitBuy" value="Buy">';
        echo '</div>';
        echo '</form>';
    }

    public function displayErrors($errors) {
        echo $errors;
    }
}