<?php

echo '<link rel="stylesheet" type="text/css" href="../../public/assets/css/style.css">';

class ProductInformations
{
    public function display($informations)
    {
        $imgPath = '../../public/assets/img/covers/' . $informations['image'];

        echo '<img src=' . $imgPath . ' class="informationImage">';

        echo '<div class="productInformations">';
        echo '<h1>' . 'Pokemon '. $informations['title'] . '</h1>';
        echo '<p class="description"><br>' . nl2br($informations['description']) . '</p>';
        echo '<div class="specifications">';
        echo '<p>Generation: ' . $informations['generation'] . '</p>';
        echo '<p>Release date: ' . $informations['release_date'] . '</p>';
        echo '<p>Console: ' . $informations['console'] . '</p>';
        echo '<p>Series: ' . $informations['series'] . '</p>';
        echo '<p>Seller note: ' . $informations['seller_note'] . '/10' . '</p>';
        echo '<p>Price: ' . $informations['selling_price'] . '€</p>';
        echo '</div>';
        echo '</div>';
    }
}