<?php

echo '<link rel="stylesheet" type="text/css" href="../public/assets/css/style.css">';

class RegisterForm
{
    public $errors = [];

    public function __construct()
    {

    }

    public function display()
    {
        echo '<div class="form">';
        echo '<h1>Register</h1>';
        echo '<form method="post">';
        echo '<input type="email" name="email" placeholder="Email" value="" required>';
        echo '<input type="password" name="password" placeholder="Password" value="" required>';
        echo '<input type="text" name="firstname" placeholder="Firstname" value="" required>';
        echo '<input type="text" name="lastname" placeholder="Lastname" value="" required>';
        echo '<input type="text" name="gender" placeholder="Gender" value="" required>';

        echo '<select name="avatar" required>';
        echo '<option value="mew">Mew</option>';
        echo '<option value="mewtwo">Mewtwo</option>';
        echo '<option value="entei">Entei</option>';
        echo '<option value="raikou">Raikou</option>';
        echo '<option value="suicune">Suicune</option>';
        echo '<option value="ho_oh">Ho oh</option>';
        echo '<option value="celebi">Celebi</option>';
        echo '<option value="rayquaza">Rayquaza</option>';
        echo '<option value="jirachi">Jirachi</option>';
        echo '<option value="manaphy">Manaphy</option>';
        echo '<option value="shaymin">Shaymin</option>';
        echo '<option value="victini">Victini</option>';
        echo '</select><br>';

        echo '<input type="submit" name="submit" value="Submit" class="submit">';

        echo '<a href="login" class="loginRegisterButton"><br>Login</a>';
        echo '</div>';
    }

    public function displayErrors()
    {
        if (!empty($this->errors)) {
            foreach ($this->errors as $error) {
                echo '<p style="color: red;">' . $error . '</p>';
            }
        }
    }

    public function displaySuccess()
    {
        echo '<p style="color: green;">User created !</p>';
    }
}
