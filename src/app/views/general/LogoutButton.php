<?php

class LogoutButton
{
    public function display($loginPath)
    {
        echo '<a href="' . $loginPath .'"><button class="button" id="logoutButton">Logout</button></a>';
    }
}