<?php

class Mail
{
    const MAIL_TYPE_ORDER_USER = 1;
    const MAIL_TYPE_ORDER_WEBMASTER = 2;

    protected $content;

    public function send() {
        echo '<br><br>E-mail sent to the '. $this->content;
    }

    public function setContent($type, $parameters)
    {
        $content = '';
        switch ($type) {
            case self::MAIL_TYPE_ORDER_USER:
                $content = $this->getOrderUserContent($parameters);
                break;
            case self::MAIL_TYPE_ORDER_WEBMASTER:
                $content = $this->getOrderWebmasterContent($parameters);
                break;
        }
        $this->content = $content;
    }

    private function getOrderUserContent($parameters)
    {
        $detail_order = '';

        foreach ($parameters['games'] as $game) {
            $detail_order .= $game['quantity'] . ' - ' . $game['title'] . ' - ' . $game['quantity'] * $game['selling_price'] . '€<br>';
        }

        return 'user:<br><br>Dear ' . $parameters['name'] . ',

        <br><br>Thank you for your recent purchase with us. We are pleased to confirm that your order has been received and is being processed.<br> We appreciate your business and look forward to providing you with the highest level of service.

        Your order details are:<br><br>
        ' . $detail_order . '
        Total Price: ' . $parameters['total_price'] . '€

        <br><br>Please note that your purchase will be shipped to the address provided during checkout:
        ' . $parameters['shipping_address'] . '
        <br>If this is incorrect, or if you would like to make any changes to the shipping address, please let us know as soon as possible.

        <br>Once again, thank you for choosing our company and we hope you enjoy your purchase. If you have any questions or concerns, please don t hesitate to contact us at ' . $parameters['support_email'] . '

        <br><br>Best regards,
        <br>The Team.<br><br>';
    }

    private function getOrderWebmasterContent($parameters)
    {
        $detail_order = '';

        foreach ($parameters['games'] as $game) {
            $detail_order .= $game['quantity'] . ' - ' . $game['title'] . ' - ' . $game['quantity'] * $game['selling_price'] . '€<br>';
        }

        return 'webmaster:<br><br>A new order has been placed on the website.

        Order details:<br><br>
        ' . $detail_order . '
        Total Price: ' . $parameters['total_price'] . '€
        <br><br>Billing Number: ' . $parameters['billing_id'] . '
        <br>Customer Name: ' . $parameters['name'] . '
        <br>Shipping Address: ' . $parameters['shipping_address'];
    }
}