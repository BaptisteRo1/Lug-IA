<?php

class Billing
{
    protected $id;
    protected $creation_date;
    protected $total_price = 0;
    protected $games = [];

    public function billingToArray()
    {
        return [
            'creation_date' => $this->creation_date,
            'total_price' => $this->total_price,
            'games' => $this->games
        ];
    }

    public function addGame($gameId)
    {
        require_once 'Game.php';

        if (isset($this->games[$gameId])) {
            $this->games[$gameId] += 1;
        } else {
            $this->games[$gameId] = 1;
        }

        $this->calculateTotalPrice();
    }

    public function removeGame($gameId)
    {
        if (isset($this->games[$gameId]) && $this->games[$gameId] > 0) {
            $this->games[$gameId] -= 1;
        }

        $this->calculateTotalPrice();
    }

    public function isInBasket($gameId)
    {
        return isset($this->games[$gameId]);
    }

    public function noGamesInBasket()
    {
        return array_sum($this->games) == 0;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTotalPrice() {
        return $this->total_price;
    }

    public function getCreationDate() {
        return $this->creation_date;
    }

    public function getGames() {
        return $this->games;
    }

    public function setInfos($infos) {
        $this->id = $infos['id'];
        $this->creation_date = $infos['creation_date'];
        $this->total_price = $infos['total_price'];
    }

    public function setGames($games) {
        if($games != null) {
            foreach ($games as $game) {
                for($i = 0; $i < $game['quantity']; $i++) {
                    $this->addGame($game['game_id']);
                }
            }
        }
    }

    private function calculateTotalPrice()
    {
        require_once 'Database.php';
        $database = new Database();
        $totalPrice = 0;

        foreach ($this->games as $gameId => $quantity) {
            $totalPrice += $database->getSellingPriceById($gameId) * $quantity;
        }

        $this->total_price = $totalPrice;
    }

    public function resetGames() {
        $this->games = [];
    }
}