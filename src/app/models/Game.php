<?php

class Game
{
    protected $id;
    protected $title;
    protected $purchase_price;
    protected $selling_price;
    protected $description;
    protected $release_date;
    protected $console;
    protected $generation;
    protected $series;
    protected $seller_note;
    protected $image;

    public function gameToArray()
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'purchase_price' => $this->purchase_price,
            'selling_price' => $this->selling_price,
            'description' => $this->description,
            'release_date' => $this->release_date,
            'console' => $this->console,
            'generation' => $this->generation,
            'series' => $this->series,
            'seller_note' => $this->seller_note,
            'image' => $this->image
        ];
    }

    public static function arrayToGame($array)
    {
        $game = new Game();
        $game->setId($array['id']);
        $game->setTitle($array['title']);
        $game->setPurchasePrice($array['purchase_price']);
        $game->setSellingPrice($array['selling_price']);
        $game->setDescription($array['description']);
        $game->setReleaseDate($array['release_date']);
        $game->setConsole($array['console']);
        $game->setGeneration($array['generation']);
        $game->setSeries($array['series']);
        $game->setSellerNote($array['seller_note']);
        $game->setImage($array['image']);
        return $game;
    }

    public static function getSortOption($games, $sortOption)
    {
        $oldestGame = $games[0];
        foreach ($games as $game)
        {
            if($game->getReleaseDate() < $oldestGame->getReleaseDate()) {
                $oldestGame = $game;
            }
        }

        if($sortOption == 'generation') {
            return $oldestGame->getGeneration();
        } else if($sortOption == 'series') {
            return $oldestGame->getSeries();
        } else {
            return $oldestGame->getConsole();
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getPurchasePrice()
    {
        return $this->purchase_price;
    }

    public function getSellingPrice()
    {
        return $this->selling_price;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getReleaseDate()
    {
        return $this->release_date;
    }

    public function getConsole()
    {
        return $this->console;
    }

    public function getGeneration()
    {
        return $this->generation;
    }

    public function getSeries()
    {
        return $this->series;
    }

    public function getSellerNote()
    {
        return $this->seller_note;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function setPurchasePrice($purchase_price)
    {
        $this->purchase_price = $purchase_price;
    }

    public function setSellingPrice($selling_price)
    {
        $this->selling_price = $selling_price;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function setReleaseDate($release_date)
    {
        $this->release_date = $release_date;
    }

    public function setConsole($console)
    {
        $this->console = $console;
    }

    public function setGeneration($generation)
    {
        $this->generation = $generation;
    }

    public function setSeries($series)
    {
        $this->series = $series;
    }

    public function setSellerNote($seller_note)
    {
        $this->seller_note = $seller_note;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }
}