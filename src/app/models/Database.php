<?php

class Database
{
    private $dsn = 'mysql:host=localhost;dbname=lug-ia';
    private $username = 'root';
    private $password = 'secret';
    private $pdo;

    public function __construct()
    {
        $this->connect();
    }

    public function connect()
    {
        try {
            $this->pdo = new PDO($this->dsn, $this->username, $this->password);

        } catch (PDOException $e) {
            echo 'Connection failed: ' . $e->getMessage();
        }
    }

    public function login($email, $password)
    {
        return $this->query('SELECT * FROM users WHERE email = :email AND password = :password',
            ['email' => $email, 'password' => $password]);
    }

    public function isAdmin($email)
    {
        $result = $this->query('SELECT * FROM administrators WHERE user_email = :email', ['email' => $email]);

        if ($result) return true;
        else return false;
    }

    public function existingGame($title)
    {
        $result = $this->query('SELECT * FROM games WHERE title = :title', ['title' => $title]);

        if($result) {return true;}
        else {return false;}
    }

    public function existingGameById($id)
    {
        $result = $this->query('SELECT * FROM games WHERE id = :id', ['id' => $id]);

        if($result) {return true;}
        else {return false;}
    }

    public function existingUser($email)
    {
        $result = $this->query('SELECT * FROM users WHERE email = :email', ['email' => $email]);

        if ($result) return true;
        else return false;
    }

    public function addUser($params)
    {
        $this->query('INSERT INTO users (password, email, first_name, last_name, gender, avatar) 
            VALUES (:password, :email, :first_name,  :last_name, :gender, :avatar)', $params);
    }

    public function updateUser($userInfos)
    {
        $this->query('UPDATE users SET first_name = :first_name, last_name = :last_name, 
                 gender = :gender, avatar = :avatar WHERE email = :email', $userInfos);
    }

    public function existingBilling($userEmail)
    {
        $query = 'SELECT * FROM billings WHERE user_email = :user_email AND placed = 0';
        return $this->query($query, ['user_email' => $userEmail]);
    }

    public function existingBasket($billing_id, $game_id)
    {
        $query = 'SELECT * FROM baskets WHERE billing_id = :billing_id AND game_id = :game_id';
        $result = $this->query($query, ['billing_id' => $billing_id, 'game_id' => $game_id]);

        if($result) {return true;}
        else {return false;}
    }

    public function getBilling($userEmail)
    {
        if($this->existingBilling($userEmail) == null) {
            $query = 'INSERT INTO billings (creation_date, user_email, total_price, placed) VALUES (:creation_date, :user_email, :total_price, :placed)';
            $stmt = $this->pdo->prepare($query);
            $params = [
                'creation_date' => date('Y-m-d H:i:s'),
                'user_email' => $userEmail,
                'total_price' => 0,
                'placed' => 0
            ];
            $stmt->execute($params);
        }

        return $this->existingBilling($userEmail);
    }

    public function getBillingGames($idBilling)
    {
        $query = 'SELECT game_id, quantity FROM baskets WHERE billing_id = :billing_id';
        $stmt = $this->pdo->prepare($query);
        $stmt->execute(['billing_id' => $idBilling]);
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function addGame($params)
    {
        $this->query('INSERT INTO games (title, purchase_price, selling_price, description, release_date, console,
            generation, series, seller_note, image) 
            VALUES (:title, :purchasePrice, :sellingPrice, :description,  :releaseDate, :console, :generation, :series,
            :sellerNote, :image)', $params);
    }

    public function addGameToBasket($billing_id, $game_id)
    {
        if($this->existingBasket($billing_id, $game_id)) {
            $query = 'UPDATE baskets SET quantity = quantity + 1 WHERE billing_id = :billing_id AND game_id = :game_id';
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(['billing_id' => $billing_id, 'game_id' => $game_id]);
        }
        else {
            $query = 'INSERT INTO baskets (billing_id, game_id, quantity) VALUES (:billing_id, :game_id, :quantity)';
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(['billing_id' => $billing_id, 'game_id' => $game_id, 'quantity' => 1]);
        }
    }

    public function removeGameFromBasket($billing_id, $game_id)
    {
        $query = 'UPDATE baskets SET quantity = quantity - 1 WHERE billing_id = :billing_id AND game_id = :game_id';
        $stmt = $this->pdo->prepare($query);
        $stmt->execute(['billing_id' => $billing_id, 'game_id' => $game_id]);
        $query = 'DELETE FROM baskets WHERE billing_id = :billing_id AND game_id = :game_id AND quantity = 0';
        $stmt = $this->pdo->prepare($query);
        $stmt->execute(['billing_id' => $billing_id, 'game_id' => $game_id]);
    }

    public function updateBasketPrice($billing_id, $total_price)
    {
        $query = 'UPDATE billings SET total_price = :total_price WHERE id = :billing_id';
        $stmt = $this->pdo->prepare($query);
        $stmt->execute(['total_price' => $total_price, 'billing_id' => $billing_id]);
    }

    public function getGamesInBasket($billing_id)
    {
        $query = 'SELECT game_id, quantity FROM baskets WHERE billing_id = :billing_id';
        $stmt = $this->pdo->prepare($query);
        $stmt->execute(['billing_id' => $billing_id]);
        $games = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach($games as $key => $game) {
            $query = 'SELECT title, selling_price FROM games WHERE id = :game_id';
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(['game_id' => $game['game_id']]);
            $gameInfos = $stmt->fetch(PDO::FETCH_ASSOC);
            $games[$key]['title'] = $gameInfos['title'];
            $games[$key]['selling_price'] = $gameInfos['selling_price'];
        }
        return $games;
    }

    public function getGame($gameId)
    {
        return $this->query('SELECT * FROM games WHERE id = :id', ['id' => $gameId]);
    }

    public function getGameId($gameTitle)
    {
        return $this->query('SELECT id FROM games WHERE title = :title', ['title' => $gameTitle]);
    }

    public function getGames()
    {
        if(isset($_POST['search'])) {
            $search = $_POST['search'];
        } else $search = '';

        $query = 'SELECT * FROM games WHERE title LIKE :search';

        if (isset($_POST['generation'])) {
            $query .= ' ORDER BY generation, series, release_date';
        }
        else if (isset($_POST['console'])){
            $query .= ' ORDER BY console, series, release_date';
        }
        else {
            $query .= ' ORDER BY series, release_date';
        }

        $stmt = $this->pdo->prepare($query);
        $stmt->execute(['search' => '%' . $search . '%']);

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getSellingPriceById($id)
    {
        $result = $this->query('SELECT selling_price FROM games WHERE id = :id', ['id' => $id]);
        return $result['selling_price'];
    }

    public function getPurchasePriceById($id)
    {
        $result = $this->query('SELECT purchase_price FROM games WHERE id = :id', ['id' => $id]);
        return $result['purchase_price'];
    }

    public function getStock($game_title)
    {
        $query = 'SELECT remaining_stock FROM stocks WHERE game_title = :game_title';

        $stmt = $this->pdo->prepare($query);
        $stmt->execute(['game_title' => $game_title]);
        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        if($result) {
            return $result['remaining_stock'];
        } else return 0;
    }

    public function addToBasket($gameId, $userEmail)
    {
        $this->query('INSERT INTO baskets (game_id, user_email) VALUES (:gameId, :userEmail)',
            ['gameId' => $gameId, 'userEmail' => $userEmail]);
    }

    public function fullfillStocks() {
            $providerId = '';
            $stock = '';

            for($i = 0; $i < 100; $i++) {
                if($this->existingGameById($i)) {
                    $game = $this->getGame($i);
                    if($game['series'] == 'Main games') {
                        $providerId = '1';
                        $stock = '20';
                    } else if ($game['series'] == '3D Battle') {
                        $providerId = '5';
                        $stock = '5';
                    } else if ($game['series'] == 'Mystery Dungeon') {
                        $providerId = '3';
                        $stock = '15';
                    } else if ($game['series'] == 'Pokepark') {
                        $providerId = '4';
                        $stock = '10';
                    } else if ($game['series'] == 'Ranger') {
                        $providerId = '2';
                        $stock = '10';
                    }

                    $query = 'INSERT INTO stocks (provider_id, game_title, year, remaining_stock, inventory_sold, total_earned, total_spent) 
                    VALUES (:provider_id, :game_title, :year, :remaining_stock, :inventory_sold, :total_earned, :total_spent)';
                    $stmt = $this->pdo->prepare($query);
                    $params = [
                        'provider_id' => $providerId,
                        'game_title' => $game['title'],
                        'year' => date('Y'),
                        'remaining_stock' => $stock,
                        'inventory_sold' => 0,
                        'total_earned' => 0,
                        'total_spent' => 0
                    ];
                    $stmt->execute($params);
                }
            }
        }

    public function addToStocks($game_id) {
        $game = $this->getGame($game_id);

        $query = 'UPDATE stocks SET remaining_stock = remaining_stock + 1 WHERE game_title = :game_title';
        $stmt = $this->pdo->prepare($query);
        $stmt->execute(['game_title' => $game['title']]);

        $query = 'UPDATE stocks SET total_spent = total_spent + :purchase_price WHERE game_title = :game_title';
        $stmt = $this->pdo->prepare($query);
        $stmt->execute(['purchase_price' => $game['purchase_price'], 'game_title' => $game['title']]);
    }

    public function removeFromStocks($game_id) {
        $game = $this->getGame($game_id);

        $query = 'UPDATE stocks SET remaining_stock = remaining_stock - 1 WHERE game_title = :game_title';
        $stmt = $this->pdo->prepare($query);
        $stmt->execute(['game_title' => $game['title']]);

        $query = 'UPDATE stocks SET total_spent = total_spent - :purchase_price WHERE game_title = :game_title';
        $stmt = $this->pdo->prepare($query);
        $stmt->execute(['purchase_price' => $game['purchase_price'], 'game_title' => $game['title']]);
    }

    public function updateStockTotalSpent()
    {
        $games = $this->getGames();

        foreach($games as $game) {
            $stock = $this->getStock($game['title']);

            $query = 'UPDATE stocks SET total_spent = :total_spent WHERE game_title = :game_title';
            $stmt = $this->pdo->prepare($query);
            $params = [
                'total_spent' => $game['purchase_price'] * $stock,
                'game_title' => $game['title']
            ];
            $stmt->execute($params);
        }
    }

    public function purchaseGame($gameId, $quantity) {
        $game = $this->getGame($gameId);
        $stock = $this->getStock($game['title']);

        if($stock >= $quantity) {
            $query = 'UPDATE stocks SET remaining_stock = remaining_stock - :quantity WHERE game_title = :game_title';
            $stmt = $this->pdo->prepare($query);
            $params = [
                'quantity' => $quantity,
                'game_title' => $game['title']
            ];
            $stmt->execute($params);

            $query = 'UPDATE stocks SET inventory_sold = inventory_sold + :quantity WHERE game_title = :game_title';
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($params);

            $query = 'UPDATE stocks SET total_earned = total_earned + :total_earned WHERE game_title = :game_title';
            $stmt = $this->pdo->prepare($query);
            $params = [
                'total_earned' => $game['selling_price'] * $quantity,
                'game_title' => $game['title']
            ];
            $stmt->execute($params);
        } else {
            return "Not enough stock for ". $game['title'] . "<br>";
        }
    }

    public function updateAfterPurchase($billing) {
        $games = $billing->getGames();
        $errors = "";

        foreach($games as $idgame => $quantity) {
            $errors .= $this->purchaseGame($idgame, $quantity);
        }

        return $errors;
    }

    public function placeBilling($idBilling) {
        $query = 'UPDATE billings SET placed = 1 WHERE id = :id';
        $stmt = $this->pdo->prepare($query);
        $stmt->execute(['id' => $idBilling]);
    }

    private function getStocksByYear($year)
    {
        $query = 'SELECT * FROM stocks WHERE year = :year';
        $stmt = $this->pdo->prepare($query);
        $stmt->execute(['year' => $year]);
        return $stmt->fetchAll();
    }

    public function getAccounting($year)
    {
        $total_earned = 0;
        $total_spent = 0;
        $total_profit = 0;

        $stocks = $this->getStocksByYear($year);
        foreach($stocks as $stock) {
            $total_earned += $stock['total_earned'];
            $total_spent += $stock['total_spent'];
        }
        $total_profit = $total_earned - $total_spent;

        return [
            'year' => $year,
            'total_earned' => $total_earned,
            'total_spent' => $total_spent,
            'total_profit' => $total_profit
        ];
    }

    public function query($query, $params = [])
    {
        $stmt = $this->pdo->prepare($query);
        $stmt->execute($params);
        return $stmt->fetch();
    }

    public function disconnect()
    {
        $this->pdo = null;
    }
}