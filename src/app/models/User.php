<?php

class User
{
    protected $id;
    protected $password;
    protected $email;
    protected $firstname;
    protected $lastname;
    protected $gender;
    protected $avatar;

    public function setInfos($params = [])
    {
        $this->id = $params['id'];
        $this->password = $params['password'];
        $this->email = $params['email'];
        $this->firstname = $params['first_name'];
        $this->lastname = $params['last_name'];
        $this->gender = $params['gender'];
        $this->avatar = $params['avatar'];
    }

    public function userToarray() {
        $userArray = [];
        $userArray['email'] = $this->email;
        $userArray['first_name'] = $this->firstname;
        $userArray['last_name'] = $this->lastname;
        $userArray['gender'] = $this->gender;
        $userArray['avatar'] = $this->avatar;

        return $userArray;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getFirstname()
    {
        return $this->firstname;
    }

    public function getLastname()
    {
        return $this->lastname;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function getAvatar()
    {
        return $this->avatar;
    }

    public function setFirstName($firstname)
    {
        $this->firstname = $firstname;
    }

    public function setLastName($lastname)
    {
        $this->lastname = $lastname;
    }

    public function setGender($gender) {
        $this->gender = $gender;
    }

    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }
}
