<?php
session_start();

class Basket extends Controller
{
    protected $database;
    protected $billing;
    protected $homeButton;
    protected $orderButton;
    protected $basketInformations;
    protected $basketManagement;
    protected $orderForm;

    public function index() {
        $this->billing = $this->getBillingSession();
        $this->database = $this->model('Database');

        echo '<div class="basketManagement">';
        $this->basketInformations = $this->view('basket/BasketInformations');
        $this->basketInformations->display($this->billing->billingToArray());

        $this->basketManagement = $this->view('basket/BasketManagement');
        echo '<div class="basketGames">';
        if($this->billing->getGames() != null) {
            foreach ($this->billing->getGames() as $gameId => $quantity) {
                if($quantity > 0) {
                    echo '<div class="basketGame">';
                    $cover = $this->database->getGame($gameId)['image'];
                    $this->basketManagement->display($gameId, $quantity, '../public/basket/', $cover);
                    echo '</div>';
                }
            }
        }
        echo '</div>';
        echo '</div>';

        $this->homeButton = $this->view('general/HomeButton');
        $this->homeButton->display("home");

        $this->orderButton = $this->view('basket/OrderButton');
        $this->orderButton->display($this->billing->noGamesInBasket());
    }

    public function addGame($gameId) {
        $this->billing = $this->getBillingSession();
        $this->billing->addGame($gameId);
        $_SESSION['billing'] = serialize($this->billing);

        $database = $this->model('Database');
        $database->addGameToBasket($this->billing->getId(), $gameId);
        $database->updateBasketPrice($this->billing->getId(), $this->billing->getTotalPrice());

        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }

    public function removeGame($gameId) {
        $this->billing = $this->getBillingSession();
        $this->billing->removeGame($gameId);
        $_SESSION['billing'] = serialize($this->billing);

        $database = $this->model('Database');
        $database->removeGameFromBasket($this->billing->getId(), $gameId);
        $database->updateBasketPrice($this->billing->getId(), $this->billing->getTotalPrice());

        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }

    public function order()
    {
        $this->orderForm = $this->view('basket/OrderForm');
        $this->orderForm->display();

        $this->homeButton = $this->view('general/HomeButton');
        $this->homeButton->display("../../home");
        $this->orderVerif();
    }

    public function orderVerif()
    {
        $this->billing = $this->getBillingSession();
        $this->database = $this->model('Database');

        if(isset($_POST['submitBuy'])) {
            $errors = $this->database->updateAfterPurchase($this->billing);

            if(!empty($errors)) {
                $this->orderForm->displayErrors($errors);

            } else {
                $user = $this->getUserSession();

                $games = $this->database->getGamesInBasket($this->billing->getId());

                $params = [
                    'games' => $games,
                    'name' => $user->getFirstName() . ' ' . $user->getLastName(),
                    'total_price' => $this->billing->getTotalPrice(),
                    'shipping_address' => $_POST['address'],
                    'support_email' => 'lugia.support@gmail.com',
                ];

                $mail = $this->model('Mail');
                $mail->setContent(1, $params);

                echo '<div class="mail1">';
                $mail->send();
                echo '</div>';

                $params = [
                    'billing_id' => $this->billing->getId(),
                    'name' => $user->getFirstName() . ' ' . $user->getLastName(),
                    'games' => $games,
                    'total_price' => $this->billing->getTotalPrice(),
                    'shipping_address' => $_POST['address'],
                ];

                $mail->setContent(2, $params);

                echo '<div class="mail2">';
                $mail->send();
                echo '</div>';

                $this->database->placeBilling($this->billing->getId());
                $billingInfos = $this->database->getBilling($user->getEmail());
                $this->billing->setInfos($billingInfos);
                $this->billing->resetGames();

                $_SESSION['billing'] = serialize($this->billing);
            }
        }
    }
}
