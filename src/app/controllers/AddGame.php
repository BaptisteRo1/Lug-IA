<?php

class AddGame extends Controller
{
    protected $addGameForm;
    protected $homeButton;

    public function index() {
        $this->addGameForm = $this->view('admin/AddGameForm');
        $this->addGameForm->display();

        $this->homeButton = $this->view('general/HomeButton');
        $this->homeButton->display("home");

        if(isset($_POST['submitGame'])) {
            $this->newGame();
        }
    }

    public function newGame()
    {
        $database = $this->model('Database');

        if($database->existingGame($_POST['title'])) {
            $this->addGameForm->errors['existing'] = 'Game already exists';
            $this->addGameForm->displayErrors();
        } else {
            $params = [
                'title' => $_POST['title'],
                'purchasePrice' => $_POST['purchasePrice'],
                'sellingPrice' => $_POST['sellingPrice'],
                'description' => $_POST['description'],
                'releaseDate' => $_POST['releaseDate'],
                'console' => $_POST['console'],
                'generation' => $_POST['generation'],
                'series' => $_POST['series'],
                'sellerNote' => $_POST['sellerNote'],
                'image' => $_POST['image']];

            $database->addGame($params);
            $this->addGameForm->displaySuccess();
            $_POST['submitGame'] = false;
        }
    }
}