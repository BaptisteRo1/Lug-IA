<?php

class Register extends Controller
{
    protected $registerForm;

    public function index()
    {
        if (isset($_SESSION['user']))
        {
            header('Location: home');
        } else {
            $this->registerForm = $this->view('register/RegisterForm');
            $this->registerForm->display();
            $this->verif();
        }
    }

    public function verif()
    {
        if(isset($_POST['submit']))
        {
            $database = $this->model('Database');

            if($database->existingUser($_POST['email'])) {
                $this->registerForm->errors['existing'] = 'User already exists';
            }

            if (!empty($this->registerForm->errors)) {
                $this->registerForm->displayErrors();
            } else {
                $params = [
                    'password' => $_POST['password'],
                    'email' => $_POST['email'],
                    'first_name' => $_POST['firstname'],
                    'last_name' => $_POST['lastname'],
                    'gender' => $_POST['gender'],
                    'avatar' => $_POST['avatar']];

                $database->addUser($params);
                $mail = $this->model('Mail');


                $this->registerForm->displaySuccess();
            }
        }
    }
}