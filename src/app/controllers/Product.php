<?php
session_start();

class Product extends Controller
{
    protected $game;
    protected $quantity = 0;
    protected $billing;
    protected $homeButton;
    protected $productInformations;
    protected $basketManagement;

    public function index($gameId)
    {
        $this->billing = $this->getBillingSession();
        $gamesId = $this->billing->getGames();
        if($this->billing->isInBasket($gameId)) {
            if($gamesId[$gameId] != null) $this->quantity = $this->billing->getGames()[$gameId];
        }

        $database = $this->model('Database');
        $this->game = $database->getGame($gameId);

        $this->productInformations = $this->view('product/ProductInformations');
        $this->productInformations->display($this->game);

        echo '<div id="basketManagement">';
        $this->basketManagement = $this->view('basket/BasketManagement');
        $this->basketManagement->display($gameId, $this->quantity, '../basket/', false, 'productManagement');
        echo '</div>';

        $this->homeButton = $this->view('general/HomeButton');
        $this->homeButton->display("../home");
    }
}