<?php
session_start();

class Login extends Controller
{
    protected $loginForm;

    public function index()
    {
        if (isset($_SESSION['user']))
        {
            header('Location: home');
        } else {
            $this->loginForm = $this->view('login/LoginForm');
            $this->loginForm->display();
            $this->verif();
        }
    }

    public function verif()
    {
        if(isset($_POST['submit']))
        {
            $database = $this->model('Database');
            $usersInfos = $database->login($_POST['email'], $_POST['password']);

            if($usersInfos == null) {
                $this->loginForm->errors['nonexistent'] = 'Wrong email or password';
            }

            if (!empty($this->loginForm->errors)) {
                $this->loginForm->displayErrors();
            } else {
                $user = $this->model('User');
                $user->setInfos($usersInfos);

                $billing = $this->model('Billing');
                $database = $this->model('Database');
                $billingInfos = $database->getBilling($user->getEmail());
                $billing->setInfos($billingInfos);
                $billingGames = $database->getBillingGames($billing->getId());
                $billing->setGames($billingGames);

                $_SESSION['user'] = serialize($user);
                $_SESSION['billing'] = serialize($billing);
                header('Location: home');
            }
        }
    }
}