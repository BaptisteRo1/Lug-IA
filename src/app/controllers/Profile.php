<?php
session_start();

class Profile extends Controller
{
    protected $user;
    protected $homeButton;
    protected $logoutButton;
    protected $userInformations;

    public function index() {
        $this->user = $this->getUserSession();

        $this->userInformations = $this->view('profile/UserInformations');
        $this->userInformations->display($this->user->userToarray());

        if(isset($_POST['submit'])) {
            $this->user->setFirstName($_POST['firstname']);
            $this->user->setLastName($_POST['lastname']);
            $this->user->setGender($_POST['gender']);
            $this->user->setAvatar($_POST['avatar']);
            $this->updateUser($this->user);
            header('Location: profile');
        }

        $this->homeButton = $this->view('general/HomeButton');
        $this->homeButton->display("home");

        $this->logoutButton = $this->view('general/LogoutButton');
        $this->logoutButton->display("logout");
    }
}