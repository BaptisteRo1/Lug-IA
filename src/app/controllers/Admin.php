<?php
session_start();

class Admin extends Controller
{
    protected $user;
    protected $database;
    protected $toolBar;
    protected $stockManagement;
    protected $addGameButton;
    protected $accountingButton;

    public function index()
    {
        $this->user = $this->getUserSession();
        $this->database = $this->model('Database');
        $this->addGameButton = $this->view('admin/AddGameButton');
        $this->accountingButton = $this->view('admin/AccountingButton');

        $this->displayGames();
        $this->toolBar = $this->view('home/ToolBar');
        $this->toolBar->display($this->user->getAvatar());
        $this->addGameButton->display();
        $this->accountingButton->display();
    }

    public function addGameStock($game_id) {
        $database = $this->model('Database');
        $database->addToStocks($game_id);
        header('Location: ../../home');
    }

    public function removeGameStock($game_id) {
        $database = $this->model('Database');
        $database->removeFromStocks($game_id);
        header('Location: ../../home');
    }

    public function displayGames()
    {
        $gamesArray = $this->database->getGames();
        $this->model('Game');
        $games = [];

        foreach ($gamesArray as $gameArray) {
            $games[] = Game::arrayToGame($gameArray);
        }

        $this->stockManagement = $this->view('admin/StockManagement');
        if(isset($_POST['generation'])) {$sortOption = 'generation';}
        else if(isset($_POST['console'])) {$sortOption = 'console';}
        else {$sortOption = 'series';}

        while(count($games) > 0)
        {
            $sortedImages = [];
            $sortedIds = [];
            $stocks = [];
            $lastSortOption = Game::getSortOption($games, $sortOption);

            foreach ($games as $game)
            {
                if($sortOption == 'generation') {
                    if($game->getGeneration() == $lastSortOption) {
                        $sortedImages[] = $game->getImage();
                        $sortedIds[] = $game->getId();
                        $stocks[] = $this->database->getStock($game->getTitle());
                        $key = array_search($game, $games);
                        unset($games[$key]);
                    }
                } else if($sortOption == 'console'){
                    if($game->getConsole() == $lastSortOption) {
                        $sortedImages[] = $game->getImage();
                        $sortedIds[] = $game->getId();
                        $stocks[] = $this->database->getStock($game->getTitle());
                        $key = array_search($game, $games);
                        unset($games[$key]);
                    }
                }
                else {
                    if ($game->getSeries() == $lastSortOption) {
                        $sortedImages[] = $game->getImage();
                        $sortedIds[] = $game->getId();
                        $stocks[] = $this->database->getStock($game->getTitle());
                        $key = array_search($game, $games);
                        unset($games[$key]);
                    }
                }
            }
            $this->stockManagement->display($lastSortOption, $sortedImages, $sortedIds, $stocks);
            $games = array_values($games);
        }
    }
}