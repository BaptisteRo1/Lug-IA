<?php
session_start();

class Home extends Controller
{
    protected $database;
    protected $gameList;
    protected $toolBar;

    public function index()
    {
        if (!isset($_SESSION['user']))
        {
            header('Location: login');
        } else {
            $user = $this->getUserSession();
            $this->database = $this->model('Database');

            if ($this->database->isAdmin($user->getEmail())) {
                header('Location: admin');
            } else {
                $this->displayGames();

                $this->toolBar = $this->view('home/ToolBar');
                $this->toolBar->display($user->getAvatar());
            }
        }
    }

    public function displayGames()
    {
        $gamesArray = $this->database->getGames();
        $game = $this->model('Game');
        $games = [];

        foreach ($gamesArray as $gameArray) {
            $games[] = Game::arrayToGame($gameArray);
        }

        $this->gameList = $this->view('home/GameImages');
        if(isset($_POST['generation'])) {$sortOption = 'generation';}
        else if(isset($_POST['console'])) {$sortOption = 'console';}
        else {$sortOption = 'series';}

        while(count($games) > 0)
        {
            $sortedImages = [];
            $sortedIds = [];
            $available = [];
            $lastSortOption = Game::getSortOption($games, $sortOption);

            foreach ($games as $game)
            {
                if($sortOption == 'generation') {
                    if($game->getGeneration() == $lastSortOption) {
                        if($this->database->getStock($game->getTitle()) > 0) {$available[] = true;}
                        else {$available[] = false;}
                        $sortedImages[] = $game->getImage();
                        $sortedIds[] = $game->getId();
                        $key = array_search($game, $games);
                        unset($games[$key]);
                    }
                } else if($sortOption == 'console'){
                    if($game->getConsole() == $lastSortOption) {
                        if($this->database->getStock($game->getTitle()) > 0) {$available[] = true;}
                        else {$available[] = false;}
                        $sortedImages[] = $game->getImage();
                        $sortedIds[] = $game->getId();
                        $key = array_search($game, $games);
                        unset($games[$key]);
                    }
                }
                else {
                    if ($game->getSeries() == $lastSortOption) {
                        if($this->database->getStock($game->getTitle()) > 0) {$available[] = true;}
                        else {$available[] = false;}
                        $sortedImages[] = $game->getImage();
                        $sortedIds[] = $game->getId();
                        $key = array_search($game, $games);
                        unset($games[$key]);
                    }
                }
            }
            $this->gameList->display($lastSortOption, $sortedImages, $sortedIds, $available);
            $games = array_values($games);
        }
    }

    public function fullfillStocks() {
        $this->database = $this->model('Database');
        $this->database->fullfillStocks();
        header('Location: home');
    }
}