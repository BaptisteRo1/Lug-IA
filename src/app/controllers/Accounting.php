<?php

class Accounting extends Controller
{
    protected $accountingSelector;
    protected $homeButton;
    protected $database;

    public function index()
    {
        $this->database = $this->model('Database');
        $this->accountingSelector = $this->view('admin/AccountingSelector');

        $this->homeButton = $this->view('general/HomeButton');
        $this->homeButton->display("home");

        if(isset($_POST['accounting'])) {
            $this->database = $this->model('Database');
            $year = $_POST['accountingYear'];
            $results = $this->database->getAccounting($year);
            $this->accountingSelector->display($results);
            unset($_POST['accounting']);

        } else {
            $this->accountingSelector->display();
        }
    }

    public function account() {
        $this->database = $this->model('Database');
        $year = $_POST['accountingYear'];

        $this->results = $this->database->getAccounting($year);
    }
}