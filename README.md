
Il s'agit de créer un site marchant de vos articles aux choix sur Internet. Notre
magasin virtuel sera en réalité une base de données permettant de stocker les
articles, de gérer le stock et les commandes effectuées par un internaute.
Soignez l’ergonomie et votre présentation qui sera prise en compte lors de
l’évaluation. Avant de rendre votre travail assurez vous que tout marche bien.
Aucun « framework » n’est autorisé. Par contre, si vous maitrisez Javascript et
jQuery, ce peut être un plus pour votre projet.
Le travail est à rendre de préférence avant le début des vacances de Noël mais
la date limite de rendu est le 21 janvier.
Le site doit se composer de deux parties :

1. Une zone publique où figurera la présentation des articles du magasin
virtuel. D'après le contenu de la table « produit », un internaute pourra
effectuer son choix et ajouter sa sélection dans un panier. La gestion du
panier consistera à stocker les produits à commander, indiquer ou
modifier leur quantité ou les supprimer. A la fin du shopping le client
pourra passer commande. Il sera identifié par son nom, prénom et email.
Chaque commande passée sera stockée dans la table « facturation » du
magasin et un mail sera envoyé au webmaster du magasin. Un accusé
d'envoi de commande apparaîtra sur l'écran du client et un mail décrivant
la commande sera envoyé à ce dernier.

2. Une zone dédiée à l'administration du magasin virtuel est constituée par
plusieurs tables. L'administration des tables consiste à ajouter, rechercher
ou supprimer un élément dans une table.

La table clients relative aux données des clients.
La table fournisseurs relative aux données des fournisseurs.
La table « produit » contient pour chaque article la quantité disponible en stock.
Les produits épuisés seront visibles par les internautes mais ne pourront pas être
commandés.

La table « produit » sera constituée des champs suivants :
- Référence : Choisie parmi une liste d'après (marque, type, aspect, sexe... - voir champs cités ci-dessus)
- Identifiant : Un nombre unique généré lors de l'ajout de la chaussure dans la base de données « produit »
- Champs relatifs à la description du produit (taille, couleur, spécifications techniques… par exemple)
- Prix Public
- Prix Achat
- Image(s) du produit (400 x 300) - Facultatif
- Icône de présentation du produit (30 x 30) - Facultatif
- Titre du produit
- Descriptif du produit 

La table « facturation » sera constituée des champs suivants :
- Identifiant du panier
- Date de création
- Nom, Prénom, email de l'acheteur
- La liste des produits se trouvant dans le panier de commande, chaque liste sera constituée du triplet (référence du produit, quantité du produit, prix du produit).
- Prix total à payer (HT et TTC, la TVA étant calculée sur la base de 20%)

La table « gestion_stock » permettra de gérer l’état du stock et de passer commande lorsqu’un article atteint un seuil critique, le webmaster sera averti par message à la connexion au site. Enfin, il faudra gérer la comptabilité des produits, c’est à dire de lister sur une durée définie à partir du début de l’année :
- Les ventes et le chiffre d’affaires réalisé.
- Les achats et leurs montant.
- Le bénéfice ou le déficit.